import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WitrazeComponent } from './witraze.component';

describe('WitrazeComponent', () => {
  let component: WitrazeComponent;
  let fixture: ComponentFixture<WitrazeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WitrazeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WitrazeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
