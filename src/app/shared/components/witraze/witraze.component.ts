import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild, Output, OnChanges, EventEmitter, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';





@Component({
  selector: 'app-witraze',
  templateUrl: './witraze.component.html',
  styleUrls: ['./witraze.component.scss']
})
export class WitrazeComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('mainDescription', { static: false }) mainDescription: ElementRef<HTMLElement>;
  @ViewChild('textDescription', { static: false }) textDescription: ElementRef<HTMLElement>;
  @ViewChild('plButton', { static: false }) plButton: ElementRef<HTMLElement>;
  @ViewChild('enButton', { static: false }) enButton: ElementRef<HTMLElement>;


  interval;
  isScrolling = false;
  clickTimeout;
  noScrollTime = 40000;
  scrollSpeed = 80;
  scrollOffset;
  counter = 0;
  goToHome;
  lang;
  loop = 0;
  backToTopTimeout;
  backToTopScroll;
  scrollTimeout;

  constructor(private router: Router,
    private translate: TranslateService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.clickTimeout = setTimeout(() => {
      this.scrollOffset = this.textDescription.nativeElement.getBoundingClientRect().height - this.mainDescription.nativeElement.getBoundingClientRect().height;
      this.scroll();

    }, this.noScrollTime);

    this.ilumination()
  }
  enLanguage(lang: string) {
    this.plButton.nativeElement.classList.remove('languageActive');
    this.enButton.nativeElement.classList.add('languageActive');
    this.translate.use('en')
    lang = "en";
    localStorage.setItem('lang', lang)
    this.stopScrolling()

    return lang
  }
  plLanguage(lang: string) {
    this.plButton.nativeElement.classList.add('languageActive');
    this.enButton.nativeElement.classList.remove('languageActive');

    lang = "pl";
    this.translate.use('pl')

    localStorage.setItem('lang', lang)
    this.stopScrolling()

    return lang

  }

  ilumination() {

    let curentLanguage = localStorage.getItem('lang')
    if (curentLanguage == "en") {

      this.plButton.nativeElement.classList.remove('languageActive');
      this.enButton.nativeElement.classList.add('languageActive');
    }

    else {

      this.plButton.nativeElement.classList.add('languageActive');
      this.enButton.nativeElement.classList.remove('languageActive');

    }
  };
  stopScrolling() {

    this.loop = 0;

    this.isScrolling = false;
    clearInterval(this.interval);
    clearTimeout(this.clickTimeout);
    clearTimeout(this.backToTopTimeout);
    clearTimeout(this.backToTopScroll);
    clearTimeout(this.scrollTimeout);

    this.clickTimeout = setTimeout(() => {
      this.scrollOffset = this.textDescription.nativeElement.getBoundingClientRect().height - this.mainDescription.nativeElement.getBoundingClientRect().height;
      this.backToTop();
    }, this.noScrollTime);
  }

  goHome() {

    this.stopScrolling()
    this.router.navigate(['/home']),
      this.translate.use('pl')
    localStorage.setItem('lang', this.lang);
    this.plButton.nativeElement.classList.add('languageActive');
    this.enButton.nativeElement.classList.remove('languageActive');

  }

  scroll() {
    this.loop++;
    if (this.loop == 8) {
      this.goHome()
    }

    else {
      this.isScrolling = true;
      this.interval = setInterval(() => {
        if (this.isScrolling) {
          if (this.counter < this.scrollOffset) {
            this.counter++;
          } else {
            clearInterval(this.interval);
            this.scrollTimeout = setTimeout(() => {
              this.isScrolling = false;
              this.backToTop();
            }, this.noScrollTime);
          }
          this.mainDescription.nativeElement.scrollTo(0, this.counter);
        }
      }, this.scrollSpeed);
    }
  };

  backToTop() {
    this.counter = 0;
    this.mainDescription.nativeElement.style.opacity = '0';

    this.backToTopTimeout = setTimeout(() => {
      this.mainDescription.nativeElement.scrollTo(0, 0);
      this.mainDescription.nativeElement.style.opacity = '1';
    }, 500);

    if (!this.isScrolling) {
      this.backToTopScroll = setTimeout(() => {
        this.scroll();
      }, this.noScrollTime);
    }
  };

  ngOnDestroy(): void {
    clearInterval(this.interval);
    clearTimeout(this.clickTimeout);
    clearTimeout(this.backToTopTimeout);
    clearTimeout(this.backToTopScroll);
    clearTimeout(this.scrollTimeout);
  }
}