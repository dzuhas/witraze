import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlytkiComponent } from './plytki.component';

describe('PlytkiComponent', () => {
  let component: PlytkiComponent;
  let fixture: ComponentFixture<PlytkiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlytkiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlytkiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
