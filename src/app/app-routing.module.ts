import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './shared/components/home/home.component';
import { PlytkiComponent } from './shared/components/plytki/plytki.component';
import { PageNotFoundComponent } from './shared/components';
import { WitrazeComponent } from './shared/components/witraze/witraze.component';



const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'witraze', component: WitrazeComponent},
  {path: 'plytki', component: PlytkiComponent},
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }),
   
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
