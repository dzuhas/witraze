import { Component, ElementRef, ViewChild, AfterViewChecked, MissingTranslationStrategy, AfterViewInit } from '@angular/core';
import { ElectronService } from './core/services';
import { WitrazeComponent } from './shared/components/witraze/witraze.component';
import { ScrollService } from './scroll.service';
import { TranslateService } from '@ngx-translate/core';
import { APP_CONFIG } from '../environments/environment';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

})
export class AppComponent implements AfterViewInit {
lang

  



  constructor(

    private electronService: ElectronService,
    private translate: TranslateService,
    public scrollService: ScrollService
  ) {

    this.lang = "pl";
    localStorage.setItem('lang', this.lang)
    this.translate.use(localStorage.getItem('lang'))
    console.log('APP_CONFIG', APP_CONFIG);

    if (electronService.isElectron) {
      console.log(process.env);
      console.log('Run in electron');
      console.log('Electron ipcRenderer', this.electronService.ipcRenderer);
      console.log('NodeJS childProcess', this.electronService.childProcess);
    } else {
      console.log('Run in browser');
    }


  }
  ngOnInit() {

  }
  ngAfterViewInit() {
    // child is set
    // this.parentFunction()

  }
  ngAfterViewChecked() {

  }

}



