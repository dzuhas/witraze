# App dedicated for touchable screen(1080x1920) in Dominikan Museum in Krakow. The application is designed to provide information about exhibition. Functionalities:

- 3 text pages
- touch and gestures
- switch language
- autoscrool and back to the top
- after 4 minutes back to home page and polish language 

## Tech Stack

**Client:** Angular 14, SCSS, HTML5, ngx-translate

**Server:** Node, angular-electron

## Demo

https://vimeo.com/775865286/2b0b15a02d

https://i.iplsc.com/muzeum-dominikanow-w-klasztorze-dominikanow-w-krakowie/000GE55BRI8LD0WK-C116-F4.webp
